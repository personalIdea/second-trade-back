# -*- coding: utf-8 -*-

from flask import Flask
from flask.ext.pymongo import PyMongo

def create_app():
    app = Flask(__name__, static_url_path='')

    # 登录注册路由
    from login_register_api import login_register_api
    app.register_blueprint(login_register_api, url_prefix='/login/user')

    # 创建商品
    from goods_create_api import goods_create_api
    app.register_blueprint(goods_create_api, url_prefix='/login/create')

    # 根据条件筛选
    from search_goods_api import search_goods_api
    app.register_blueprint(search_goods_api, url_prefix='/login/search')

    # 编辑商品信息
    from edit_goods_api import edit_goods_api
    app.register_blueprint(edit_goods_api, url_prefix='/login/edit')

    # 购物车存储
    from shopping_cars_api import shopping_cars_api
    app.register_blueprint(shopping_cars_api, url_prefix='/login/car')

    # 订单详情处理
    from order_detail_api import order_detail_api
    app.register_blueprint(order_detail_api, url_prefix='/login/order')

    return app