# -*- coding:utf-8 -*-

from flask import request
from common import mongo, make_response
from . import goods_create_api
from bson import ObjectId

@goods_create_api.route('/create_goods/<operation>', methods=['POST'])
def create_goods(operation):
    data = request.get_json(force=True)
    if operation == 'edit':
        check = mongo()['goods_description'].find_one({'_id': ObjectId(data['_id'])})
        if check == None:
            return make_response(
                status=2,
                msg=u'编辑失败，无此商品'
            )
        else:
            data['_id'] = ObjectId(data['_id'])
            mongo()['goods_description'].find_one_and_update({'_id': ObjectId(data['_id'])}, {'$set': data})
            return make_response(
                status=0,
                msg=u'编辑成功'
            )
    elif operation == 'create':
        check = mongo()['goods_description'].find_one({'shopName': data['shopName']})
        if check == None:
            data['status'] = 1 # 创建上架的状态
            mongo()['goods_description'].insert_one(data)
            return make_response(
                status=0,
                msg=u'创建商品成功'
            )
        else:
            return make_response(
                status=2,
                msg=u'创建失败，该产品已有人上线，请重新命名'
            )

@goods_create_api.route('/test', methods=['GET'])
def index():
    return 'hjkl'