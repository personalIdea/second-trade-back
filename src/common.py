# -*- coding: utf-8 -*-

from flask import jsonify
from pymongo import MongoClient

def make_response(data = None, status = 0, msg = 'OK', http_status = 200):
    response = jsonify({
        'data': data,
        'status': status,
        'msg': msg
    })
    response.status_code = http_status
    return response

# 连接数据库
def mongo():
    mongo = MongoClient(['localhost:27017']) # 连接数据库：host和port
    db = mongo.practice # 库名
    return db