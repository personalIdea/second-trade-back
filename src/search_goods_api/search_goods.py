# -*- coding: utf-8 -*-

from . import search_goods_api
from common import make_response, mongo
from flask import request
from bson import ObjectId
import re

@search_goods_api.route('/<username>', methods=['GET'])
def query_goods_index(username):
    shopName = request.args.get('shopName')
    shopType = request.args.get('shopType')
    priceStart = request.args.get('priceStart')
    if username == 'user':
        query_goods = mongo()['goods_description'].find({'status': 1})
    elif username == 'admin':
        username = request.args.get('user')
        query_goods = mongo()['goods_description'].find({'shopAuth':username})
    goods_box = []
    for member in query_goods:
        if shopType == u'全部':
            shopType = ''
        if re.search(shopName, member['shopName'])!=None and re.search(shopType, member['shopType'])!=None:
            if priceStart == u'全部':
                member['_id'] = str(member['_id'])
                goods_box.append(member)
            elif priceStart == '1000' and member['shopPrice'] > int(priceStart)+200:
                member['_id'] = str(member['_id'])
                goods_box.append(member)
            elif int(priceStart) < member['shopPrice'] < int(priceStart)+200:
                member['_id'] = str(member['_id'])
                goods_box.append(member)
    if query_goods == None:
        return make_response(
            status=0,
            msg=u'无此商品'
        )
    else:
        return make_response(
            status=0,
            data=goods_box
        )

@search_goods_api.route('/member', methods=['GET'])
def query_good_member():
    shopId = request.args.get('_id')
    query_good = mongo()['goods_description'].find_one({'_id': ObjectId(shopId)})
    same_goods_check = mongo()['goods_description'].find({'shopType': query_good['shopType']})
    if query_good == None:
        return make_response(
            status=2,
            msg=u'未找到该商品'
        )
    else:
        query_good['_id'] = str(query_good['_id'])
        same_goods = []
        for member in same_goods_check:
            member['_id'] = str(member['_id'])
            if len(same_goods) < 5 and query_good['_id'] != member['_id']:
                same_goods.append(member)
        return make_response(
            status=0,
            data={
                'detail': query_good,
                'same_style': same_goods
            }
        )

@search_goods_api.route('/test', methods=['GET'])
def test():
    return 'ghjk'