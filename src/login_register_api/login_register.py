# -*- coding: utf-8 -*-

from . import login_register_api
from flask import request
from common import make_response
from common import mongo
import re

@login_register_api.route('/register', methods=['POST'])
def user_register():
    req_obj = request.get_json(force=True).get('email')
    password = request.get_json(force=True).get('password')
    user = mongo().users.find_one({'username': req_obj})
    if user == None:
        mongo().users.insert_one({
            'username': req_obj,
            'authority': 'administrator',
            'password': password
        })
        return make_response(
            status=0,
            http_status=200,
            msg=u'注册成功'
        )
    else:
        return make_response(
            status=4,
            http_status=200,
            msg=u'抱歉，您的账号已被注册过'
        )

@login_register_api.route('/get', methods=['POST'])
def get_login():
    req_obj = request.get_json(force=True).get('email')
    user = mongo().users.find_one({'username': req_obj}, {'password': 0, '_id': 0})
    return make_response(
        status=0,
        data=user
    )

@login_register_api.route('/check', methods=['POST'])
def user_login():
    req_obj = request.get_json(force=True).get('email')
    password = request.get_json(force=True).get('password')
    user = mongo().users.find_one({'username': req_obj, 'password': password}, {'password': 0, '_id': 0})
    try:
        if user == None:
            return make_response(
                status=-1,
                msg=u'登录失败，账户不存在或者密码错误',
                http_status=200
            )
        else:
            try:
                num = len(mongo()['shopping_car'].find_one({'shopAuth': user['username']})['goods'])
            except Exception,e:
                num = 0
            user['car_num'] = num
            return make_response(
                status=0,
                msg=u'登录成功',
                data=user,
                http_status=200,
            )
    except Exception,e:
        return make_response(
            status=-1,
            msg=u'token失效'
        )

@login_register_api.route('/edit', methods=['POST'])
def edit_user():
    req_obj = request.get_json(force=True)
    user = mongo().users.find_one({'username': req_obj['username'], 'password': req_obj['password']}, {'_id': 0})
    print req_obj.get('phone')
    if user == None:
        return make_response(
            status='-1',
            msg=u'密码错误'
        )
    else:
        if  req_obj.get('phone'):
            p2 = re.compile('^0\d{2,3}\d{7,8}$|^1[358]\d{9}$|^147\d{8}')
            phonematch = p2.match(req_obj['phone'])
            if phonematch:
                print 11;
            else:
                return make_response(
                    status=3,
                    msg=u'手机号码错误'
                )
        try:
            req_obj['password'] = req_obj['firstNew']
            del req_obj['firstNew']
            del req_obj['secondNew']
            mongo().users.find_one_and_update({'username': req_obj['username'], 'password': user['password']}, {
                '$set': req_obj
            })
        except Exception,e:
            mongo().users.find_one_and_update({'username': req_obj['username'], 'password': user['password']}, {
                '$set': req_obj
            })
        return make_response(
            status=0,
            data=mongo().users.find_one({'username': req_obj['username'], 'password': req_obj['password']}, {'password': 0, '_id': 0}),
            msg=u"修改成功"
        )

@login_register_api.route('/test', methods=['GET'])
def ojj():
    return 'fjdkl'