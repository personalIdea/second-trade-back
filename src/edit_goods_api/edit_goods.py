# -*- coding: utf-8 -*-

from . import edit_goods_api
from common import make_response, mongo
from flask import request
from bson import ObjectId

@edit_goods_api.route('/goods', methods=['POST'])
def edit_goods():
    return 'hjk'


# 上架、下架、删除
@edit_goods_api.route('/<operation>', methods=['GET'])
def delete_up_down(operation):
    if operation == 'delete':
        id = request.args.get('id')
        mongo()['goods_description'].find_one_and_delete({'_id': ObjectId(id)})
        return make_response(
            status=0,
            msg=u'删除成功'
        )
    elif operation == 'up':
        id = request.args.get('id')
        mongo()['goods_description'].find_one_and_update({'_id': ObjectId(id)}, {'$set': {'status': 1}})
        return make_response(
            status=0,
            msg=u'上架成功'
        )
    elif operation == 'down':
        id = request.args.get('id')
        mongo()['goods_description'].find_one_and_update({'_id': ObjectId(id)}, {'$set': {'status': 0}})
        return make_response(
            status=0,
            msg=u'下架成功'
        )

@edit_goods_api.route('/test',methods=['GET'])
def test():
    return 'hjklhjkl'