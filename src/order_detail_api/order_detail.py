# -*- coding: utf-8 -*-

from flask import request
from common import make_response, mongo
from . import order_detail_api


@order_detail_api.route('/get', methods=['GET'])
def get_all_order():
    check = mongo()['order_detail'].find({})
    getOrder = []
    for member in check:
        member['_id'] = str(member['_id'])
        getOrder.append(member)
    return make_response(
        status=0,
        data=getOrder
    )