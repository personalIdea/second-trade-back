# -*- coding: utf-8 -*-

from flask import request
from common import make_response, mongo
from . import shopping_cars_api
from bson import ObjectId

@shopping_cars_api.route('/add', methods=['POST'])
def add_cars():
    user = request.get_json(force=True)
    try:
        check = mongo()['shopping_car'].find_one({'shopAuth': user['users']})
        if check == None:
            mongo()['shopping_car'].insert_one({
                'shopAuth': user['users'],
                'goods':[{
                    'goods_id': user['goods_id'],
                    'goods_num': user['num']
                }]
            })
        else:
            checkSame = 0
            for index, member in enumerate(check['goods']):
                if member['goods_id'] == user['goods_id']:
                    check['goods'][index]['goods_num'] = int(check['goods'][index]['goods_num']) + user['num']
                    checkSame = 1
            if checkSame == 0:
                check['goods'].append({
                        'goods_id': user['goods_id'],
                        'goods_num': user['num']
                    })
            mongo()['shopping_car'].find_one_and_update({'shopAuth': user['users']}, {'$set': {'goods': check['goods']}})
        return make_response(
            status=0,
            msg=u'加入购物车成功',
            data=len(mongo()['shopping_car'].find_one({'shopAuth': user['users']})['goods'])
        )
    except Exception,e:
        return make_response(
            status="-1",
            msg=u"账户未登录，不可加入购物车"
        )


@shopping_cars_api.route('/get', methods=['GET'])
def get_car_goods():
    userName = request.args.get('username')
    car = mongo()['shopping_car'].find_one({'shopAuth': userName})
    goods_array = []
    try:
        for member in car['goods']:
            goods = mongo()['goods_description'].find_one({'_id': ObjectId(member['goods_id'])})
            goods['_id'] = str(goods['_id'])
            goods_array.append({
                'num': member['goods_num'],
                'detail': goods
            })
    except Exception,e:
        goods_array = []
    return make_response(
        status=0,
        data=goods_array
    )

@shopping_cars_api.route('/delete', methods=['GET'])
def delete_cars():
    userName = request.args.get('username')
    carId = request.args.get('id')
    car = mongo()['shopping_car'].find_one({'shopAuth': userName}, {'goods': 1})
    for index, member in enumerate(car['goods']):
        if member['goods_id'] == carId:
            del car['goods'][index]
    print car['goods']
    mongo()['shopping_car'].find_one_and_update({'shopAuth': userName}, {'$set': {'goods': car['goods']}})
    return make_response(
        status=0,
        msg=u'移除商品成功',
        data=len(mongo()['shopping_car'].find_one({'shopAuth': userName})['goods'])
    )

@shopping_cars_api.route('/pay', methods=['POST'])
def pay_goods():
    data = request.get_json(force=True)
    shopping = mongo()['shopping_car'].find_one({'shopAuth': data['username']})
    record = 0
    for index, member in enumerate(data['goods']):
        del shopping['goods'][index - record]
        record = record + 1
        check = mongo()['goods_description'].find_one({'_id': ObjectId(member['id'])})
        check['_id'] = str(check['_id'])
        member['detail'] = check
        if check.get('payPeople') == None:
            mongo()['goods_description'].find_one_and_update({'_id': ObjectId(member['id'])}, {'$set':{'payPeople': 1,
                                                                                                       'shopNumber': check['shopNumber'] - member['num']}})
        else:
            mongo()['goods_description'].find_one_and_update({'_id': ObjectId(member['id'])}, {'$set': {'payPeople': check['payPeople'] + 1,
                                                                                                        'shopNumber':check['shopNumber'] - member['num']}})
    if len(shopping['goods']) == 0:
        mongo()['shopping_car'].find_one_and_delete({'shopAuth': data['username']})
    else:
        mongo()['shopping_car'].find_one_and_update({'shopAuth': data['username']}, {'$set': {'goods': shopping['goods']}})
    mongo()['order_detail'].insert_one(data)
    try:
        num = len(mongo()['shopping_car'].find_one({'shopAuth': data['username']})['goods'])
    except Exception,e:
        num = 0
    return make_response(
        status=0,
        msg=u'支付成功',
        data=num
    )